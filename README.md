Test Application based on Symfony 2.6.*@dev version
========================

1) Installation
----------------------------------

After (cloning / downloading) project you must execute these steps to start testing project

### Steps

#### 1) Install vendor dependencies

If you don't have Composer yet, download it following the instructions on http://getcomposer.org/ or just run the following command:

    curl -s http://getcomposer.org/installer | php
    
Then you need to install all the necessary dependencies. Run the following command:

    php composer.phar install
    
#### 2) Prepare database

You need to create a database first:

    php app/console doctrine:database:create

Then you need to load database schema:

	php app/console doctrine:schema:update --force

#### 3) Run the server

You can use built-in php web server to test application (or you can use nginx/apache but individual configuration is needed)

	php app/console server:run

Then visit address

	http://localhost:8000/




All libraries and bundles included in the Symfony Standard Edition are
released under the MIT or BSD license.

Enjoy!