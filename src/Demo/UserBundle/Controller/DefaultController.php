<?php

namespace Demo\UserBundle\Controller;

use Demo\UserBundle\Entity\User;
use Demo\UserBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package Demo\UserBundle\Controller
 * @author Aurimas Niekis <aurimas.niekis@gmail.com>
 */
class DefaultController extends Controller
{
    public function loginAction()
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirect($this->generateUrl(''));
        }

        $helper = $this->get('security.authentication_utils');

        return $this->render('DemoUserBundle:Default:login.html.twig', [
            'last_username' => $helper->getLastUsername(),
            'error' => $helper->getLastAuthenticationError(),
        ]);
    }

    public function signUpAction()
    {
        $user = new User();

        $form = $this->createSignUpForm($user);

        return $this->render('DemoUserBundle:Default:sign_up.html.twig', [
            'form' => $form->createView()
        ]);
    }

    protected function createSignUpForm(User $user)
    {
        $form = $this->createUserForm($user, 'demo_user_create');

        $form->add('submit', 'submit', array('label' => 'Sign Up'));

        return $form;
    }

    protected function createUserForm(User $user, $route, $edit = false)
    {
        return $this->createForm(new UserType($edit ? $user : null), $user, [
            'action' => $this->generateUrl($route),
            'method' => 'POST'
        ]);
    }

    public function editAction()
    {
        $user = $this->getUser();

        $form = $this->createEditForm($user);

        return $this->render('DemoUserBundle:Default:edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    protected function createEditForm(User $user)
    {
        $form = $this->createUserForm($user, 'demo_user_update', true);

        $form->add('submit', 'submit', array('label' => 'Save'));

        return $form;
    }

    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getUser();

        $originalPassword = $entity->getPassword();

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if (!empty($entity->getPassword())) {
                $plainTextPassword = $entity->getPassword();

                $encoder = $this->get('security.password_encoder');
                $password = $encoder->encodePassword($entity, $plainTextPassword);

                $entity->setPassword($password);
            } else {
                $entity->setPassword($originalPassword);
            }

            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('demo_site_index');
        }

        return $this->render('DemoUserBundle:Default:edit.html.twig', array(
            'entity' => $entity,
            'form' => $editForm->createView()
        ));
    }

    public function createUserAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = new User();
        $form = $this->createSignUpForm($user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $plainTextPassword = $user->getPassword();

            $encoder = $this->get('security.password_encoder');
            $password = $encoder->encodePassword($user, $plainTextPassword);

            $user->setPassword($password);

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('demo_user_login');
        }

        return $this->render('DemoUserBundle:Default:sign_up.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
