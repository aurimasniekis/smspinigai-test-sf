<?php

namespace Demo\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class DefaultController
 * @package Demo\SiteBundle\Controller
 * @author Aurimas Niekis <aurimas.niekis@gmail.com>
 */
class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('DemoSiteBundle:Default:index.html.twig');
    }
}
