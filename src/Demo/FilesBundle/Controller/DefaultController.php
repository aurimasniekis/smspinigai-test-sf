<?php

namespace Demo\FilesBundle\Controller;

use Demo\FilesBundle\Entity\File;
use Demo\FilesBundle\Form\FileType;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DefaultController
 * @package Demo\FilesBundle\Controller
 * @author Aurimas Niekis <aurimas.niekis@gmail.com>
 */
class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $fileRepo = $em->getRepository('DemoFilesBundle:File');
        $user = $this->getUser();

        $files = $fileRepo->findByUser($user);

        return $this->render(
            'DemoFilesBundle:Default:index.html.twig',
            [
                'files' => $files
            ]
        );
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $fileRepo = $em->getRepository('DemoFilesBundle:File');

        try {
            $file = $fileRepo->findOneBy(
                [
                    'user' => $this->getUser(),
                    'id' => $id
                ]
            );
        } catch (NoResultException $e) {
            throw new NotFoundHttpException('File not found');
        }

        $em->remove($file);
        $em->flush();

        return $this->redirect($this->generateUrl('demo_files_list'));
    }

    public function newAction()
    {
        $user = $this->getUser();

        if ($user->getFiles()->count() >= 20) {
            return $this->redirect($this->generateUrl('demo_files_list'));
        }

        $file = new File();
        $file->setUser($user);

        $form = $this->createForm(
            new FileType(),
            $file,
            [
                'action' => $this->generateUrl('demo_files_upload'),
                'method' => 'POST'
            ]
        );

        return $this->render(
            'DemoFilesBundle:Default:new.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    public function uploadAction(Request $request)
    {
        $user = $this->getUser();

        if ($user->getFiles()->count() >= 20) {
            return $this->redirect($this->generateUrl('demo_files_list'));
        }

        $file = new File();
        $file->setUser($user);

        $form = $this->createForm(
            new FileType(),
            $file,
            [
                'action' => $this->generateUrl('demo_files_upload'),
                'method' => 'POST'
            ]
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($file);
            $em->flush();

            return $this->redirect($this->generateUrl('demo_files_list'));
        }

        return $this->render(
            'DemoFilesBundle:Default:new.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
